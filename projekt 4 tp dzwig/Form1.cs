﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace projekt_4_tp_dzwig
{
    public partial class Form1 : Form
    {
        Hook hook1;
        Crane crane1, load;
        Arm arm1;
        
        public struct Crane
        {
            public short length,
                         height;

        }
        public struct Arm
        {
            public short length,
                         height;
        }

        public struct Block
        {
            public int
                  height,
                  length,
                  shapex,
                  shapey;
            public bool isSquare;
        }
        public struct Hook
        {
            public int 
                  length,
                  height;
        }
        int actual = 0;

        public Form1()
        {
            InitializeComponent();
                
           
            hook1.height = 10;
            crane1.length = 50;
            crane1.height = 300;
            load.length = 50;
            load.height = 20;
            arm1.length = 600;
             hook1.length = 10 + crane1.length + 65;
            arm1.height = load.height;
            blocks = Initialize_Blocks();
        }
        
        public Block[] Initialize_Blocks()
        {
            Random rnd = new Random();

            Block[] blocks= new Block[8] ;
            for (short i = 0; i < blocks.Length; i++)
            {


                int number = rnd.Next(1, 11);
                if (number % 5 == 0)
                {
                    if (number > 5)
                    {
                        blocks[i].shapex = 10;
                        blocks[i].isSquare = false;
                    }
                    else
                    {
                        blocks[i].shapey = 10;
                        blocks[i].isSquare = false;
                    }

                }
                else {
                    blocks[i].isSquare = true;
                }

                blocks[i].length = 50+ 60*i + crane1.length ;
                 blocks[i].height= crane1.height;
            }
            return blocks;
        }
        bool isGrabbed = false;
        
        public void Grab_block()
        {

            for (short i = 0; i < blocks.Length; i++)
            {
                if(hook1.height>=blocks[i].height-100 && blocks[i].isSquare == true
                   &&hook1.length-20>=blocks[i].length
                   &&hook1.length<=blocks[i].length+49)
                {
                            isGrabbed = true;
                            actual = i;
                            break;
                      
                }
            }
        }
        public void Put_Block()
        {
            for (int i = 0; i < blocks.Length; i++){
                if(blocks[actual].height+50==blocks[i].height
                    && blocks[actual].length == blocks[i].length
                    )
                {
                    for (int j = 0; j < blocks.Length; j++)
                    {
                        if(blocks[actual].length!=blocks[j].length&&blocks[actual].height!=blocks[j].height)
                        isGrabbed = false;
                    }
                }

            }
        }
     
    private void Form1_Load(object sender, EventArgs e)
        {
            this.DoubleBuffered = true;
            this.Paint += new PaintEventHandler(panel1_Paint);
        }

        private PaintEventHandler Block_Paint(Block[] block)
        {
            throw new NotImplementedException();
        }
        Block[] blocks;
        private void panel1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            


            //load
            System.Drawing.SolidBrush pen = new System.Drawing.SolidBrush(System.Drawing.Color.DarkCyan);
            e.Graphics.FillRectangle(pen, new Rectangle(10, 50-8, load.length, load.height+16));

            //crane
             pen = new System.Drawing.SolidBrush(System.Drawing.Color.Yellow);
            e.Graphics.FillRectangle(pen, new Rectangle(load.length, 25, crane1.length, crane1.height));
            pen = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
            e.Graphics.FillRectangle(pen, new Rectangle(crane1.length, 50, arm1.length,
                arm1.height));

            //hook
            pen = new System.Drawing.SolidBrush(System.Drawing.Color.Orange);
            e.Graphics.FillRectangle(pen, new Rectangle(hook1.length,
                60, 
                10,
                10+hook1.height));
            
           
            pen = new System.Drawing.SolidBrush(System.Drawing.Color.Blue);

            //blocks
            for (short i = 0; i < 8; i++)
            {

                e.Graphics.FillRectangle(pen,
                    //new Rectangle(60 * i + crane1.length + blocks[i].length + blocks[i].shapex,
                    //crane1.height+ blocks[i].shapey+blocks[i].height-50,
                    new Rectangle(blocks[i].length, blocks[i].height,
                    50 - blocks[i].shapex,
                    50 - blocks[i].shapey));
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (hook1.height >10)
                
            {
                hook1.height -= 50;
                Grab_block();
                if(isGrabbed == true)
                {
                    blocks[actual].height -= 50;
                   
                }
                panel1.Refresh();
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (hook1.height < crane1.height - 50)
            {
                hook1.height += 50;
                
                if (isGrabbed == true)
                {
                    blocks[actual].height += 50;

                }
                if(blocks[actual].height>150) Put_Block();

                panel1.Refresh();
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            if (hook1.length > load.length+100)
            {
                hook1.length -= 60;
                if (isGrabbed == true)
                {
                    blocks[actual].length-= 60;
                }
                panel1.Refresh();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (hook1.length<arm1.length- crane1.length-load.length+50)
            { 
            hook1.length += 60;
                if (isGrabbed == true)
                {
                    blocks[actual].length += 60;
                }
                panel1.Refresh();
            }
        }

        
    }
}
